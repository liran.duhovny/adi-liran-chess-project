#include "bishop.h"
#include <cmath>
#define BISHOP 'B'
#define MOVEONEAWONE 1
int bishop::checkMove(Point dest)
{
    if (abs(this->_location.getX()- dest.getX()) ==  abs(this->_location.getY()- dest.getY()))//check if move in oblique line
    {
        std::vector<Point> a = std::vector<Point>();
        this->getPointsBetween(a, dest);//gets the points between
        for (int i = 1; i < a.size(); i++)
        {
            if (this->_curr->getTool(a[i]) != nullptr)
            {
                return ERR;//wheather is there points were our tools located.
            }
        }
        return MOOV;
    }
    return ERR;
}

bishop::bishop(Point location, char color, board* gameBoard) : tool(BISHOP, color, location, gameBoard)
{//constrctor
}

void bishop::getPointsBetween(std::vector<Point>& a, Point dest)
{//send to static function.
    bishop::generalGetPointsBetween(a, dest, this);
}

bishop::~bishop()
{//destructor
}

void bishop::generalGetPointsBetween(std::vector<Point>& a, Point dest, tool* myTool)
{//add points to path of the bishop
    //there are four ways of running obliquelly
    Point myPlace = Point(myTool->getPosition().getX(), myTool->getPosition().getY());
    while (dest.getX() < myPlace.getX() && dest.getY() < myPlace.getY())
    {
        a.push_back(myPlace);
        myPlace.setX(myPlace.getX() - MOVEONEAWONE);
        myPlace.setY(myPlace.getY() - MOVEONEAWONE);
    }
    while (dest.getX() > myPlace.getX() && dest.getY() > myPlace.getY())
    {
        a.push_back(myPlace);
        myPlace.setX(myPlace.getX() + MOVEONEAWONE);
        myPlace.setY(myPlace.getY() + MOVEONEAWONE);
    }
    while (dest.getY() < myPlace.getY() && dest.getX() > myPlace.getX())
    {
        a.push_back(myPlace);
        myPlace.setY(myPlace.getY() - MOVEONEAWONE);
        myPlace.setX(myPlace.getX() + MOVEONEAWONE);

    }
    while (dest.getY() > myPlace.getY() && dest.getX() < myPlace.getX())
    {
        a.push_back(myPlace);
        myPlace.setX(myPlace.getX() - MOVEONEAWONE);
        myPlace.setY(myPlace.getY() + MOVEONEAWONE);
    }
}
