#pragma once
#include "tool.h"
class Pawn : public tool
{
protected:
	bool _isEnpatientable;
public:
	virtual int checkMove(Point dest);
	//c'tor
	Pawn(Point location, char color, board* gameBoard);
	virtual void getPointsBetween(std::vector<Point>& a, Point dest);
	void setIsEnPatien(bool setter);
	//d'tor
	~Pawn();


};