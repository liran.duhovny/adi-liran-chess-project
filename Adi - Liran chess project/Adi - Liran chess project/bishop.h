#pragma once
#pragma once
#pragma once
#include <iostream>
#include "tool.h"
class bishop : public tool
{
public:
	virtual int checkMove(Point dest);
	//c'tor
	bishop(Point location, char color, board* gameBoard);
	virtual void getPointsBetween(std::vector<Point>& a, Point dest);
	//d'tor
	~bishop();
	static void generalGetPointsBetween(std::vector<Point>& a, Point dest, tool* myTool);

};