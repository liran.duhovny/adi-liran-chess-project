#include "board.h"
#include "king.h"
#include "rook.h"
#include "bishop.h"
#include "Queen.h"
#include "Knight.h"
#include "Pawn.h"
#define BOARDSIZE 64
#define BOARDRAW 8
#define MOVEBYONE 1
#define CORRECTMOVESHAH 1
board::board(std::string gameInitial)
{//baord definer with chars in like arry obj
	this->_gameBoard = new tool*[BOARDSIZE];
	for  (int i = 0; i < BOARDRAW ; i++)
	{
		for (int j = 0; j < BOARDRAW ; j++)
		{
			switch (gameInitial[BOARDRAW * i + j])
			{
			case '#'://adds to aray like object the currnt baord if chae equal to null it wont add it 
				this->_gameBoard[i * BOARDRAW + j] = nullptr;
				break;
			case 'k':
				this->_gameBoard[i * BOARDRAW + j] = new king(Point(j, i), 'b', this);//add by given points name and its peramiters
				break;
			case 'K':
				this->_gameBoard[i * BOARDRAW + j] = new king(Point(j, i), 'w', this);
				break;
			case 'q':
				this->_gameBoard[i * BOARDRAW + j] = new Queen(Point(j, i), 'b', this);
				break;
			case 'Q':
				this->_gameBoard[i * BOARDRAW + j] = new Queen(Point(j, i), 'w', this);
				break;
			case 'r':
				this->_gameBoard[i * BOARDRAW + j] = new rook(Point(j, i), 'b', this);
				break;
			case 'R':
				this->_gameBoard[i * BOARDRAW + j] = new rook(Point(j, i), 'w', this);
				break;
			case 'n':
				this->_gameBoard[i * BOARDRAW + j] = new Knight(Point(j, i), 'b', this);
				break;
			case 'N':
				this->_gameBoard[i * BOARDRAW + j] = new Knight(Point(j, i), 'w', this);
				break;
			case 'b':
				this->_gameBoard[i * BOARDRAW + j] = new bishop(Point(j, i), 'b', this);
				break;
			case 'B':
				this->_gameBoard[i * BOARDRAW + j] = new bishop(Point(j, i), 'w', this);;
				break;
			case 'p':
				this->_gameBoard[i * BOARDRAW + j] = new Pawn(Point(j, i), 'b', this);;
				break;
			case 'P':
				this->_gameBoard[i * BOARDRAW + j] = new Pawn(Point(j, i), 'w', this);;
				break;


			}//end of board construction 
		}
	}
}

tool* board::change(Point src, Point dst)
{
	tool* temp = this->getTool(dst);//chagne the board currnt look
	this->setTool(dst, this->getTool(src));//st tool to dest
	this->setTool(src, nullptr);//set tool to surc as null
	this->getTool(dst)->setPoisition(dst);//set dest 
	return temp;//return the tool that was at the destintaion
}

void board::cancelChange(tool* toMoveBack, tool* deleted)//undo changes
{//this function will move the tool to its original position
	this->setTool(toMoveBack->getLastPosition(), toMoveBack);
	this->setTool(toMoveBack->getPosition(), deleted);
	toMoveBack->setPoisition(toMoveBack->getLastPosition());
}

tool* board::getTool(Point dst)//return tool at the dst pos
{
	return this->_gameBoard[dst.getY() * BOARDRAW + dst.getX()];
}

tool* board::getTool(int x, int y)//return tool at point with int x and y pos
{
	return this->_gameBoard[y * BOARDRAW + x];
}

tool* board::getTool(int place)//get a tool in the place 
{//return tool ptr
	return this->_gameBoard[place];
}

//this function will chaekc the all possiable ways to cheack mate 
bool board::checkMate(char color, tool* myking)
{
	Point kingPoint = Point(myking->getPosition().getX() + MOVEBYONE, myking->getPosition().getY() + MOVEBYONE);
	//chaeking all the tools around the king and theier movment ability towatds the king
	if (kingPoint.getX() < 8 && kingPoint.getY() < 8 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setY(kingPoint.getY() - MOVEBYONE);
	if (kingPoint.getX() < 8 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setY(kingPoint.getY() - MOVEBYONE);
	if (kingPoint.getX() < 8 && kingPoint.getY() > 0 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setX(kingPoint.getX() - MOVEBYONE);
	if (kingPoint.getY() > 0 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setX(kingPoint.getX() - MOVEBYONE);
	if (kingPoint.getX() > 0 && kingPoint.getY() > 0 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setY(kingPoint.getY() + MOVEBYONE);
	if (kingPoint.getX() > 0 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setY(kingPoint.getY() + MOVEBYONE);
	if (kingPoint.getX() > 0 && kingPoint.getY() < 8 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setX(kingPoint.getX() + MOVEBYONE);
	if (kingPoint.getY() < 8 && !(checkCurrThreat(color, myking, kingPoint)) && (this->getTool(kingPoint) == nullptr || this->getTool(kingPoint)->getColor() != myking->getColor()))
	{
		return false;
	}
	kingPoint.setY(kingPoint.getY() - MOVEBYONE);
	int cnt = 0;
	std::vector<Point> a = std::vector<Point>();//add points to vector arounf the king 
	for (int i = 0; i < 8; i++)//go over board and look for therat
	{
		for (int j = 0; j < BOARDRAW; j++)
		{
			if (this->getTool(j, i) != nullptr && this->getTool(j, i)->getColor() != myking->getColor() && this->getTool(j, i)->checkMove(myking->getPosition()) == 0)
			{
				if (cnt != 0)
				{
					return true;//there is chamate
				}
				cnt++;
				this->getTool(j, i)->getPointsBetween(a, myking->getPosition());
			}
		}
	}
	for (int i = 0; i < a.size(); i++)
	{
		for (int j = 0; j < BOARDRAW; j++)
		{
			for (int k = 0; k < BOARDRAW; k++)
			{
				if ((k != myking->getPosition().getX() || j != myking->getPosition().getY()) && this->getTool(k, j) != nullptr && this->getTool(k, j)->getColor() == myking->getColor())
				{
					if (this->getTool(k, j)->checkMove(a[i]) == 0)
					{
						return false;//no therat
					}
				}
			}
		}
	}
	return true;//chaeckmate is avalible

}

bool board::checkCurrThreat(char color, tool* myking, Point newPoint)
{//this function will chaeck the threat if it is could be used 
	int result = 0;
	tool* temp = nullptr;
	if (this->getTool(newPoint) == nullptr || this->getTool(newPoint)->getColor() != color)
	{
		temp = this->change(myking->getPosition(), newPoint);
		result = this->isMyKingThreated(color, myking);
		this->cancelChange(myking, temp);
		if (result == CORRECTMOVESHAH)
		{
			return true;
		}
	}
	return false;
}

int board::isMyKingThreated(char color, tool* myKing)
{//this function will go over the board and chaeck if the king is threated or not.
	tool* temp = nullptr;
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < BOARDRAW; j++)
		{
			temp = this->getTool(j, i);
			if (temp != nullptr && temp->getColor() != color && temp->checkMove(myKing->getPosition()) != 6)
			{
				return CORRECTMOVESHAH;
			}
		}
	}
	return 0;
}

void board::clearBoard()//this function will clear the baord by setting all the vars for null ptr
{
	//clearBoardFunction
	for (int i = 0; i < BOARDRAW; i++) {
		for (int j = 0; j < BOARDRAW; j++)
		{
			this->_gameBoard[i * BOARDRAW + j] = nullptr;
		}
	}
	//end of clear board function.
}

void board::setTool(Point dest, tool* myTool)//set tool from current place to another one !only if possible!
{
	this->_gameBoard[dest.getY() * BOARDRAW + dest.getX()] = myTool;
}

//this function will fiable the enpatient
void board::disableEnpatient(char color)
{
	tool* temp = nullptr;
	for (int i = 0; i < BOARDRAW; i++)
	{
		for (int j = 0; j < BOARDRAW; j++)
		{
			temp = this->getTool(j, i);
			if (temp != nullptr && (temp->getType() == 'p' || temp->getType() == 'P') && temp->getColor() == color)
			{
				((Pawn*)(temp))->setIsEnPatien(false);
			}
		}
	}
}

board::~board()
{//dtor
	delete[] this->_gameBoard;
}
