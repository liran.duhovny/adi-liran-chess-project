#pragma once
#include <iostream>
#include "tool.h"
#include "rook.h"
#include "bishop.h"
class Queen : public tool
{
public:
	virtual int checkMove(Point dest);
	//c'tor
	Queen(Point location, char color, board* gameBoard);
	virtual void getPointsBetween(std::vector<Point>& a, Point dest);
	//d'tor
	~Queen();
	static void generalGetPointsBetween(std::vector<Point>& a, Point dest, tool* myTool);

};