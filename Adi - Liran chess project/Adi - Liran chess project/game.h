#pragma once
#include "Player.h"
#include "Pipe.h"
#include <iostream>
#include "tool.h"
class Game
{
protected:
	//board _board
	Player* _player1;
	Player* _player2;
	board* _gameBoard;
	Pipe p;
	int _turn;//define player turn right now
public:
	void gameHandle();//game maintimiter
	void printboard();//write to backend
	std::string getBaordToPipe();
	char turn();
	void switchTurn();
	void writeToScreen(); //write to frontEnd
	std::string move(std::string request);
	Game(Player* p1, Player* p2, std::string gameSTR, Pipe myPipe);
	~Game();
};