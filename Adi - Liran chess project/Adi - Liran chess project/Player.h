#pragma once
#include <iostream>
#include "king.h"
class Player //init player class!
{
protected:
	std::string _name;
	char _color;
	king* _myking;///king pointer
public:
	//setters
	king* getking();
	void setking(king* setter);
	
	//getters
	char getcolor();
	//c'tor
	Player(std::string name, char color, king* playerKing);
	//d'tor
	~Player();
};