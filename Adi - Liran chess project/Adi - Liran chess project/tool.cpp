#include "tool.h"
#define FROM_BIG_TO_SMALL 32
int tool::move(Point destPoint)
{
    //returning movement abbility to move on top of the board
    int result = this->checkMove(destPoint);
    return result;

}

void tool::setLastPoisition(Point pos)
{//set the last position of point to tool from movment of tool to know were to come back acording to error
    this->_lastLocation.setX(pos.getX());
    this->_lastLocation.setY(pos.getY());
}

void tool::setPoisition(Point pos)
{//set location acording to Point given 
    this->_location.setX(pos.getX());
    this->_location.setY(pos.getY());
}

Point tool::getPosition()
{   //returning the current location with point argument
    return (Point(this->_location.getX(), this->_location.getY()));
}

Point tool::getLastPosition()
{//return the last location acorrding to point created arguments
    return Point(this->_lastLocation.getX(), this->_lastLocation.getY());
}

char tool::getColor()
{//getter get color
    return this->_color;
}

char tool::getType()
{//getter get type
    return this->_type;
}

tool::tool(char type, char color, Point location, board* gameB)
{   //constrctor
    this->_curr = gameB;//set color and tool item by char
    this->_color = color;
    if (color == 'b')
    {
        type = type + FROM_BIG_TO_SMALL;//add to char if the color is balck to know wich tool is acording to wich player
    }
    this->_type = type;
    this->_location = Point(location.getX(), location.getY());//set point
    this->_lastLocation = Point(location.getX(), location.getY());//set last location
}

tool::~tool()//d'tor 
{
    this->_curr = nullptr;
}
