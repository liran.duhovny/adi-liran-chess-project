#pragma once
#pragma once
#include <iostream>
#include "tool.h"
class rook : public tool
{
public:
	virtual int checkMove(Point dest);
	//c'tor
	rook(Point location, char color, board* gameBoard);
	virtual void getPointsBetween(std::vector<Point>& a, Point dest);
	//d'tor
	~rook();
	static void generalGetPointsBetween(std::vector<Point>& a, Point dest, tool* myTool);
	
};