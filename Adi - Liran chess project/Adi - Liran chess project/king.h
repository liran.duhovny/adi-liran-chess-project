#pragma once
#include <iostream>
#include "tool.h"
class king : public tool
{
public:
	virtual int checkMove(Point dest);
	virtual void getPointsBetween(std::vector<Point>& a, Point dest);
	//c'tor
	king(Point location, char color, board* gameBoard);
	//d'tor
	~king();
};