#pragma once
#include "Point.h"
#include "tool.h"
class tool;
class board
{
protected:
	tool** _gameBoard;
public:
	//c'tor
	board(std::string gameInitial);
	
	tool* change(Point src, Point dst);
	void cancelChange(tool* toMoveBack, tool* deleted);
	tool* getTool(Point dst);
	tool* getTool(int x, int y);
	tool* getTool(int place);
	bool checkMate(char color, tool* myking);
	bool checkCurrThreat(char color, tool* myking, Point newPoint);
	int isMyKingThreated(char color, tool* myKing);
	void clearBoard();
	void setTool(Point dest, tool* myTool);
	void disableEnpatient(char color);
	
	//d'tor
	~board();
};