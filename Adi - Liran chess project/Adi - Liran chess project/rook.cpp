#include "rook.h"
#define  ROOK 'R'
#define MOOVEBY 1
//chaeking movement ability 
int rook::checkMove(Point dest)
{
    //charking movement acorrding to the games rules 
    if (this->_location.getX() == dest.getX() || this->_location.getY() == dest.getY())
    {
        //create vector to collect points 
        std::vector<Point> a = std::vector<Point>();
        this->getPointsBetween(a, dest);
        //go over the board and chaeck movement
        for (int i = 1; i < a.size(); i++)
        {
            if (this->_curr->getTool(a[i]) != nullptr)
            {
                return ERR;//return iliigal movement 
            }
        }
        return MOOV;//can move
    }
    return ERR;//return iliigal movement 
}

rook::rook(Point location, char color, board* gameBoard): tool(ROOK, color, location, gameBoard)
{
}//constructor 

void rook::getPointsBetween(std::vector<Point>& a, Point dest)
{//send to static function to reorgnize it
    rook::generalGetPointsBetween(a, dest, this);
}

rook::~rook()
{//d'tor
}

void rook::generalGetPointsBetween(std::vector<Point>& a, Point dest, tool* myTool)
{
    //add point to vector 
    Point myPlace = Point(myTool->getPosition().getX(), myTool->getPosition().getY());
    while (dest.getX() < myPlace.getX())
    {   //push to points
        a.push_back(myPlace);
        myPlace.setX(myPlace.getX() - MOOVEBY);
    }
    while (dest.getX() > myPlace.getX())
    {
        //push to points
        a.push_back(myPlace);
        myPlace.setX(myPlace.getX() + MOOVEBY);
    }
    while (dest.getY() < myPlace.getY())
    {
        //push to points
        a.push_back(myPlace);
        myPlace.setY(myPlace.getY() - MOOVEBY);
    }
    while (dest.getY() > myPlace.getY())
    {
        //push to points
        a.push_back(myPlace);
        myPlace.setY(myPlace.getY() + MOOVEBY);
    }
}
