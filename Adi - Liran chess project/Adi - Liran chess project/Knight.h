#pragma once
#include "tool.h"
class Knight : public tool
{
public:
	virtual int checkMove(Point dest);
	//c'tor
	Knight(Point location, char color, board* gameBoard);
	virtual void getPointsBetween(std::vector<Point>& a, Point dest);
	//d'tor
	~Knight();


};