/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/
#include <cstring>
#include "Pipe.h"
#include <iostream>
#include <thread>
#include "game.h"
#include <string>
#include "Player.h"
using std::cout;
using std::endl;
using std::string;
using std::cin;

void main()
{
	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	Game g = Game(new Player("b",'w',nullptr), new Player("a", 'b', nullptr), "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1", p);
	std::string req = "";
	string str_obj = g.getBaordToPipe();
	strcpy_s(msgToGraphics, str_obj.c_str());
	p.sendMessageToGraphics(msgToGraphics);
	g.printboard();
	string msgFromGraphics = p.getMessageFromGraphics();
	
	/*p.sendMessageToGraphics(msgToGraphics);*/   // send the board string
	while (req != "quit")
	{	
		string str_obj = g.move(msgFromGraphics);
		strcpy_s(msgToGraphics, str_obj.c_str());
		p.sendMessageToGraphics(msgToGraphics);
		//g.printboard();
		msgFromGraphics = p.getMessageFromGraphics();

	}
	p.close(); 

}