//Point cpp file, using class Point
#include "Point.h"
#include <iostream>
#define DEFULTVAL 0
//constructor
Point::Point()
{	//set points x and y 
	this->_x = DEFULTVAL;
	this->_y = DEFULTVAL;
}


//set points (setter)
Point::Point(int x, int y)
{
	this->setX(x);
	this->setY(y);
}
//set point by string
Point::Point(std::string location)
{
	this->setPoint(location);
}
//d'tor
Point::~Point()
{
}
//get x val of point
int Point::getX()
{
	return this->_x;
}

//set x val of point
void Point::setX(int x)
{
	this->_x = x;
}

//get y val of point
int Point::getY()
{
	return this->_y;
}

//set y val to point
void Point::setY(int y)
{
	this->_y = y;
}
//set point with string given.
void Point::setPoint(std::string location)
{
	this->setX(location[0] - 'a');//set x (used with chars )
	this->setY(8-(location[1] - '0'));//sett y
}
