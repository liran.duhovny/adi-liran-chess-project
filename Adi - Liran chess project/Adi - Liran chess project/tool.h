#pragma once
#include "Point.h"
#include "board.h"
#include <string>
#include <iostream>
#include <vector>
#define  MOOV 0
#define  ERR 6
class board;
class tool
{
protected:
	char _type;
	char _color;
	Point _location;
	Point _lastLocation;	
	board* _curr;
public:
	virtual int move(Point destPoint);
	virtual int checkMove(Point dest) = 0;
	virtual void getPointsBetween(std::vector<Point>& a, Point dest) = 0;
	//seters
	void setLastPoisition(Point pos);
	void setPoisition(Point pos);
	//getters
	Point getPosition();
	Point getLastPosition();
	char getColor();
	char getType();
	//constractor / D'tor
	tool(char type, char color, Point location, board* gameB);
	~tool();
};