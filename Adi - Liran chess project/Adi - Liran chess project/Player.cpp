#include "Player.h"
#include <iostream>

//king getter.
king* Player::getking()
{
	return this->_myking;
}
//king setter
void Player::setking(king* setter)
{
	this->_myking = setter;
}
//collor getter
char Player::getcolor()
{
	return this->_color;
}
Player::Player(std::string name, char color, king* playerKing)
{
	//constarctor
	this->_name = name;//setter name color and king pointer 
	this->_color = color;
	this->_myking = playerKing;
}

//d'tor
Player::~Player()
{
	this->_myking = nullptr;
}