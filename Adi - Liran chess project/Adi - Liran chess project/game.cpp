#include "game.h"
using std::cout;
using std::endl;
using std::cin;//define opcodes and board elements
#define BOARDRAWORCOL 8
#define BOARDSIZE 64
#define PLAYERZERO 0
#define PLAYERONE 1
#define MOVEOUTOFBOARD 5
#define MOVETOSAMEPOINT 7
#define NOTOOL 2
#define DEST_COLOR_IS_SOURCE_COLOR 3
#define ILIGALMOVE 6
#define CHASE 1
#define EN_PATIENT 1
#define CHACHMAT 8
#define LEAGAlMOOV 0
void Game::gameHandle()
{//gmae handler
	

}

void Game::printboard()//craet cmd board print
{	
	tool* temp = nullptr;
	cout << "  a b c d e f g h" << endl;//board chars 
	for (int i = 0; i < BOARDRAWORCOL; i++)//used as y location param
	{
		cout << 8 - i << " ";
		for (int j = 0; j < BOARDRAWORCOL; j++)//used as x location param
		{
			temp = this->_gameBoard->getTool(j, i);
			if (temp == nullptr)
			{
				cout << "# ";//add # on places where there is no players
			}
			else
			{
				cout << temp->getType()<< " ";//add space beatwin chars
			}
		}
		cout << endl;//add \n
	}
}

std::string Game::getBaordToPipe()//sends board to pipe
{
	std::string boardStr = "";//create string
	tool* temp = nullptr;
	
	for (int i = 0; i < BOARDRAWORCOL; i++)//used as y location param
	{

		
		for (int j = 0; j < BOARDRAWORCOL; j++)//used as x location param
		{
			temp = this->_gameBoard->getTool(j, i);
			if (temp == nullptr)
			{
				boardStr += '#';//add # to places withour players or tools
			}
			else
			{
				boardStr += temp->getType();//return type
			
			}
		}
		
	}
	boardStr += '1';//add prefix
	return boardStr;
}

char Game::turn()//this function tells wich player is this turn
{
	if (this->_turn == PLAYERZERO)
	{
		return this->_player1->getcolor();//reutnrn player 1 turn
	}
	return this->_player2->getcolor();//return player 2 turn
}

void Game::switchTurn()//this function switch turns 1->0 /0->1
{
	this->_turn = (this->_turn + 1) % 2;
}

void Game::writeToScreen()//write to screen prefix
{
	cout << "printing to screen" << endl;
}

std::string Game::move(std::string request)//this function mainatns the tool's moovment ability and returns opcode for moovments.
{
	Point srcP = Point(request.substr(0, 2));
	Point dstP = Point(request.substr(2, 2));//ceack if the move is not outside the board
	if (srcP.getX() < 0 || srcP.getX() > 7 || srcP.getY() < 0 || srcP.getY() > 7 || dstP.getX() < 0 || dstP.getX() > 7 || dstP.getY() < 0 || dstP.getY() > 7)
	{
		return std::to_string(MOVEOUTOFBOARD);
	}
	if (srcP.getX() == dstP.getX() && dstP.getY() == srcP.getY())//chaeck if the src and det point are the same
	{
		return std::to_string(MOVETOSAMEPOINT);
	}
	tool* toMove = nullptr;
	toMove = this->_gameBoard->getTool(srcP);
	if (toMove == nullptr || toMove->getColor() != this->turn())//there is no tool of src point
	{
		return std::to_string(NOTOOL);
	}
	if (this->_gameBoard->getTool(dstP) !=  nullptr && this->_gameBoard->getTool(dstP)->getColor() == this->turn())//in the dest point there is tool of your color
	{
		return std::to_string(DEST_COLOR_IS_SOURCE_COLOR);
	}
	int result = toMove->move(dstP);//cheack if thte tool can do its moovment at all...
	if (result == ILIGALMOVE)
	{
		return std::to_string(result);
	}
	int chessResult = 0;
	tool* temp = this->_gameBoard->change(srcP, dstP);
	if (result == EN_PATIENT)//deleting the pawn by the en patient technique
	{
		temp = this->_gameBoard->getTool(dstP.getX(), srcP.getY());
		this->_gameBoard->setTool(Point(dstP.getX(), srcP.getY()), nullptr);
	}
	if (this->turn() == this->_player1->getcolor())
	{
		chessResult = this->_gameBoard->isMyKingThreated(this->turn(), this->_player1->getking());
	}
	else
	{
		chessResult = this->_gameBoard->isMyKingThreated(this->turn(), this->_player2->getking());
	}
	if (chessResult == CHASE)
	{
		if (result == EN_PATIENT)
		{
			this->_gameBoard->setTool(Point(dstP.getX(), srcP.getY()), temp);
			temp = nullptr;
		}
		this->_gameBoard->cancelChange(toMove, temp);
		return std::to_string(4);
	}
	if (result == EN_PATIENT)
	{
		char msgToGraphics[1024];//add # to the en patiented pawn
		std::string toGraphics = std::to_string(dstP.getX());
		toGraphics.append(std::to_string(srcP.getY()));
		toGraphics.append("#");
		strcpy_s(msgToGraphics, toGraphics.c_str());
		this->p.sendMessageToGraphics(msgToGraphics);
	}
	if (temp != nullptr)
	{
		delete temp;
	}
	this->switchTurn();
	this->_gameBoard->disableEnpatient(this->turn());
	toMove->setLastPoisition(toMove->getPosition());//set tools last location with current
	if (this->turn() != this->_player1->getcolor())
	{
		chessResult = this->_gameBoard->isMyKingThreated(this->turn(), this->_player2->getking());
		if (chessResult == CHASE)
		{
			if (this->_gameBoard->checkMate(this->turn(), this->_player2->getking()))
			{
				return std::to_string(CHACHMAT);//end the game
			}
			return std::to_string(CHASE);//opponent thearthend 

		}
	}
	else
	{
		chessResult = this->_gameBoard->isMyKingThreated(this->turn(), this->_player1->getking());
		if (chessResult == CHASE)
		{
			if (this->_gameBoard->checkMate(this->turn(), this->_player1->getking()))
			{
				return std::to_string(CHACHMAT);//end the game 
			}
			return std::to_string(CHASE);//opponent thearthend

		}
	}
	return std::to_string(LEAGAlMOOV);//leagal move

}

Game::Game(Player* p1, Player* p2, std::string gameSTR, Pipe myPipe)//constructor 
{
	this->_gameBoard = new board(gameSTR);//set board
	p1->setking((king*)this->_gameBoard->getTool(gameSTR.find('K')));//creat kings for opponents
	p2->setking((king*)this->_gameBoard->getTool(gameSTR.find('k')));//
	this->_player1 = p1;
	this->_player2 = p2;
	this->p = myPipe;
	if (gameSTR[BOARDSIZE] == '0')//set turns 
	{
		this->_turn = PLAYERZERO;
	}
	else
	{
		this->_turn = PLAYERONE;
	}
}



Game::~Game()//d'tor 
{
	delete this->_gameBoard;//delete players and game
	delete this->_player1;
	delete this->_player2;
}
