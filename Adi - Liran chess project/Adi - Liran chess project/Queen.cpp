#include "Queen.h"
#define QUEEN 'Q'

//chaeck queen movment abbility 
int Queen::checkMove(Point dest)
{
    //chaeck movment like a rook and bishop acording to the game rules
    if ((this->_location.getX() == dest.getX() || this->_location.getY() == dest.getY()) || (abs(this->_location.getX() - dest.getX()) == abs(this->_location.getY() - dest.getY())))
    {//add point to vector 
        std::vector<Point> a = std::vector<Point>();
        this->getPointsBetween(a, dest);
        for (int i = 1; i < a.size(); i++)
        {   //cheack if the movment is acorrding to the rulse
            if (this->_curr->getTool(a[i]) != nullptr)
            {
                return ERR;
            }
        }
        return MOOV;//can be mooved
    }
    return ERR;//cannot move to the chosen loaction
}

Queen::Queen(Point location, char color, board* gameBoard) : tool(QUEEN, color, location, gameBoard)
{//constructor 
}

void Queen::getPointsBetween(std::vector<Point>& a, Point dest)
{//getting to function static one to add points
    Queen::generalGetPointsBetween(a, dest, this);
}

Queen::~Queen()
{//d'tor
}

void Queen::generalGetPointsBetween(std::vector<Point>& a, Point dest, tool* myTool)
{//getting point between like rook and bishop 
    Point myPlace = Point(myTool->getPosition().getX(), myTool->getPosition().getY());
    //cheack like who the tools tries to move
    if (myPlace.getX() == dest.getX() || myPlace.getY() == dest.getY()) {
        rook::generalGetPointsBetween(a, dest, myTool);//going to rook points adder
    }
    else {
        bishop::generalGetPointsBetween(a, dest, myTool);//going to bishop points setter
    }
}
