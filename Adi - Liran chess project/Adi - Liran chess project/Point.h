#pragma once
#include <iostream>
class Point
{
protected:
	int _x;
	int _y;//location on board
	//end class definition.
public:
	Point();
	Point(int x, int y);
	Point(std::string location);
	~Point();
	int getX();
	void setX(int x);
	int getY();
	void setY(int y);
	void setPoint(std::string location);

};

